Feature: Creating a Booking as a Broker - Happy Flow

  Scenario Outline: Create a new booking as a broker

  //Step 1 - Acceptance Criteria Scenario 3: Operator requests a single leg booking
    Given An authenticated Operator
    And Payload is filled with correct data
    And Operator wants to request a single leg booking
    And Operator sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway stores the request on the blockchain
    And Gateway returns an 'update' to the Operator with the transactionID
    And Gateway sends the 'update' to the correct Broker in the correct webhook with the transactionID
    And Gateway receives a success response from the Broker

  //Step 2 - Scenario: booking starts from Operator side
    Given An authenticated Operator
    And Payload is filled with correct data
    And Operator sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway stores the request on the blockchain
    And Gateway returns an 'update' to the Operator with the transactionID
    And Gateway sends the 'update' to the correct Broker in the correct webhook with the transactionID
    And Gateway receives a success response from the Broker

  //Step 3 - Scenario: broker confirmed the flight and gateway returns it to the operator
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker verifies the 'update'
    And Broker sends the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with a new transactionID
    And Gateway sends the 'update' to the correct Operator with the new transactionID
    And Gateway receives a success response from the Operator

  //Step 4 - Test Scenario: operator receives the broker's payload back from the gateway. Then the operator needs to send it back to the gateway
  //(as an "ack"). No changes were made, so consensus was reached (verified on Gateway),and it will not need to be forwarded to the broker
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator accepted the 'update'
    And Operator sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the response
    And Gateway verifies the response
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with a new transactionID
    And Gateway finishes transaction since consensus was reached

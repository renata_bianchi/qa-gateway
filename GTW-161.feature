Feature: Updating a Booking Request as a Broker - Cancel one or more Legs

  Scenario Outline: Canceling one or more legs

  #Step 1 - Acceptance Criteria Scenario 1: broker not authenticated
    Given An unauthenticated Broker
    And Payload is filled with correct data
    And Broker sent a 'cancelStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the Broker

  #Step 2 - Acceptance Criteria Scenario 2: wrong leg ID
    Given An authenticated Broker
    And Payload is filled with a wrong leg ID
    And Broker sent an 'cancelStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  #Step 3 - Acceptance Criteria Scenario 3: wrong broker ID
    Given An authenticated Broker
    And Payload is filled with a Broker ID that doesn't belong to him
    And Broker sent the 'cancelStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  #Step 4 - Acceptance Criteria Scenario 4: leg cancelling accepted
    Given An authenticated Broker
    And Payload is filled with a CANCEL REQUEST for a leg
    And Broker sent the 'cancelStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway accepts the request
    And Gateway returns an 'update' to the broker with the 'transactionID'
    And Gateway stores the request on the database
    And Gateway sends the 'update' to the Operator with the 'transactionID'
    And Gateway receives a success response from the Broker

  #Step 5 - Scenario: cancel request received, verified and responded by the operator
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator accepted the 'update'
    And Operator sent the 'update'
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with the new 'transactionID'
    And Gateway sends the 'update' to the correct Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker

  #Step 6 - Scenario: updates accepted and verified by the Gateway. Consensus reached
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker accepted the 'update'
    And Broker sent the 'update'
    When Gateway receives the response
    And Gateway verifies the response
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with the new transactionID
    And Gateway finishes the transactions since consent was reached
    And Leg is successfully canceled
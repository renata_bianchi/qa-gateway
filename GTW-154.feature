Feature: Creating a Booking as a Broker - Alternative Flows

  Scenario Outline: Alternative flows for creating a booking as a Broker

  //Step 1 - Acceptance Criteria Scenario 1: unauthenticated broker
    Given An unauthenticated Broker
    And Payload is filled with correct data
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the user


  //Step 2 - Scenario: broken payload format
    Given An authenticated Broker
    And Payload's body is modified (a mandatory field is taken off or renamed)
    And Broker sends a 'bookingStart'
    When Gateway receives the request
    And Gateway verifies the payload
    Then Gateway notices the broken payload
    And Gateway returns a fail response to the Broker

  //Step 3 - Scenario: operator is offline
    Given An authenticated Broker
    And Payload is filled with correct data
    And Operator is offline
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway returns an 'update' to the Broker with the 'transactionID'_
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the Operator with the 'transactionID'
    And Gateway receives an error response from the Operator

  //Step 4 - Scenario: broker is offline
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator verifies the 'update'
    And Operator sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with a new 'transactionID'
    And Gateway sends the 'update' to the correct Broker with the new 'transactionID'
    And Gateway receives a fail response from the Broker

  //Step 5 -  Test Scenario: broker receives the operator's payload back from the gateway. Then the broker needs to send it back to the gateway (as an "ack").
  //Changes were made, so no consensus was reached (verified on Gateway),and it will need to be forwarded to the operator and so on, until consensus is reached
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker verifies the 'update'
    And Broker sent the 'update' back to the Gateway
    When Gateway receives the response
    And Gateway verifies the response from both Broker and Operator
    And The request and response are NOT in consensus
    Then Gateway notices the divergent responses
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'



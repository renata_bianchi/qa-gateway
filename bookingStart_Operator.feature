Feature: Create a new booking as an Operator

  Scenario Outline: Sending a bookingStart as an Operator
    Given An authenticated Operator
    And Payload is filled with:
      | Requester ID  | Operator ID  | Team ID  | Sales Person ID | Booking Reference  | Booking Date UTC | Operator Booking ID | Operator Leg ID | Departure IATA Code | Departure ICAO Code | Arrival IATA Code | Arrival ICAO Code | Aircraft Type Code | Aircraft Display Name | Aircraft Name | Tail Number  | ETA Local                     | ETA UTC                     | ETD Local                       | ETD UTC                       | Schedule Time of Arrival Local | Schedule Time of Arrival UTC | Schedule Time of Departure Local | Schedule Time of Departure UTC | Notes   | Remarks   |
      | <requesterId> | <operatorId> | <teamId> | <salesPersonId> | <bookingReference> | <bookingDateUTC> | <operatorBookingId> | <operatorLegId> | <departureIataCode> | <departureIcaoCode> | <arrivalIataCode> | <arrivalIcaoCode> | <code>             | <displayName>         | <name>        | <tailNumber> | <estimatedTimeOfArrivalLocal> | <estimatedTimeOfArrivalUtc> | <estimatedTimeOfDepartureLocal> | <estimatedTimeOfDepartureUtc> | <scheduleTimeOfArrivalLocal>   | <scheduleTimeOfArrivalUtc>   | <scheduleTimeOfDepartureLocal>   | <scheduleTimeOfDepartureUtc>   | <notes> | <remarks> |
    And User send a bookingStart
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns code "<GatewayResponse>"
    And Gateway sends the error message "<errorMessage>"

    Examples: Valid Data
      | requesterId | operatorId | teamId                                 | salesPersonId                          | bookingReference | bookingDateUTC       | operatorBookingId | operatorLegId | departureIataCode | departureIcaoCode | arrivalIataCode | arrivalIcaoCode | code | displayName | name           | tailNumber | estimatedTimeOfArrivalLocal | estimatedTimeOfArrivalUtc | estimatedTimeOfDepartureLocal | estimatedTimeOfDepartureUtc | scheduleTimeOfArrivalLocal | scheduleTimeOfArrivalUtc | scheduleTimeOfDepartureLocal | scheduleTimeOfDepartureUtc | notes                | remarks                | GatewayResponse | errorMessage |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note  | this is a remark       | 0200            | <NULL>       |
      | 55687       | 7798       | "string4456dsa89-1ce8w74-7se89a7ea"    | "string4g8e6rg7-4a798sr6rsa6r-79as87t" | 8897UPZE         | 2018-06-21T16:10:00Z | 1A                | 1A            | GRU               | SBGR              | BER             | EDDT            | B747 | Boeing 747  | Boeing 747     | BO-41A     | 2018-05-21T09:35:00Z        | 2018-05-21T11:35:00Z      | 2018-05-20T07:00:00Z          | 2018-05-20T11:00:00Z        | 2018-05-21T10:00:00Z       | 2018-05-21T12:15:00Z     | 2018-05-20T09:00:00Z         | 2018-05-20T09:00:00Z       | notes for the flight | remarks for the flight | 0200            | <NULL>       |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | <NULL>                      | <NULL>                    | <NULL>                        | <NULL>                      | <NULL>                     | <NULL>                   | <NULL>                       | <NULL>                     | please leave a note  | this is a remark       | 0200            | <NULL>       |

    Examples: Invalid Date Format
      | requesterId | operatorId | teamId                                 | salesPersonId                          | bookingReference | bookingDateUTC       | operatorBookingId | operatorLegId | departureIataCode | departureIcaoCode | arrivalIataCode | arrivalIcaoCode | code | displayName | name           | tailNumber | estimatedTimeOfArrivalLocal | estimatedTimeOfArrivalUtc | estimatedTimeOfDepartureLocal | estimatedTimeOfDepartureUtc | scheduleTimeOfArrivalLocal | scheduleTimeOfArrivalUtc | scheduleTimeOfDepartureLocal | scheduleTimeOfDepartureUtc | notes               | remarks          | GatewayResponse | errorMessage |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | INVALID              | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | INVALID                     | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | INVALID                   | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | INVALID                       | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | INVALID                     | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | INVALID                    | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | INVALID                  | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | INVALID                      | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | TBC          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | INVALID                    | please leave a note | this is a remark | 0505            | TBC          |

    Examples: Missing Data
      | requesterId | operatorId | teamId                                 | salesPersonId                          | bookingReference | bookingDateUTC       | operatorBookingId | operatorLegId | departureIataCode | departureIcaoCode | arrivalIataCode | arrivalIcaoCode | code | displayName | name           | tailNumber | estimatedTimeOfArrivalLocal | estimatedTimeOfArrivalUtc | estimatedTimeOfDepartureLocal | estimatedTimeOfDepartureUtc | scheduleTimeOfArrivalLocal | scheduleTimeOfArrivalUtc | scheduleTimeOfDepartureLocal | scheduleTimeOfDepartureUtc | notes               | remarks          | GatewayResponse | errorMessage                         |
      | <NULL>      | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | Missing requester identifier         |
      | 123         | <NULL>     | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | Missing operator identifier          |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | <NULL>            | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | Missing requester booking identifier |
      | 123         | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | <NULL>        | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0505            | Missing requester leg identifier     |

    Examples: Invalid Data
      | requesterId | operatorId | teamId                                 | salesPersonId                          | bookingReference | bookingDateUTC       | operatorBookingId | operatorLegId | departureIataCode | departureIcaoCode | arrivalIataCode | arrivalIcaoCode | code | displayName | name           | tailNumber | estimatedTimeOfArrivalLocal | estimatedTimeOfArrivalUtc | estimatedTimeOfDepartureLocal | estimatedTimeOfDepartureUtc | scheduleTimeOfArrivalLocal | scheduleTimeOfArrivalUtc | scheduleTimeOfDepartureLocal | scheduleTimeOfDepartureUtc | notes               | remarks          | GatewayResponse | errorMessage                 |
      | 123         | Unkn@wn!   | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0200            | Unknown operator identifier  |
      | Unkn@wn!    | 124        | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC              | 2018-05-15T19:45:00Z | 123               | 123           | VCP               | SBCP              | VCR             | SBGR            | A320 | Airbus 320  | Airbus A320neo | PR-AXT     | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note | this is a remark | 0200            | Unknown requester identifier |


    Examples: Number of Characters Overflow
      | requesterId         | operatorId          | teamId                                 | salesPersonId                          | bookingReference    | bookingDateUTC       | operatorBookingId   | operatorLegId       | departureIataCode | departureIcaoCode | arrivalIataCode   | arrivalIcaoCode   | code                | displayName         | name                | tailNumber          | estimatedTimeOfArrivalLocal | estimatedTimeOfArrivalUtc | estimatedTimeOfDepartureLocal | estimatedTimeOfDepartureUtc | scheduleTimeOfArrivalLocal | scheduleTimeOfArrivalUtc | scheduleTimeOfDepartureLocal | scheduleTimeOfDepartureUtc | notes                 | remarks               | GatewayResponse | errorMessage                                                          |
      | <256+ chars string> | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field requester identifier is over 255 characters        |
      | 123                 | <256+ chars string> | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field operator identifier is over 255 characters         |
      | 123                 | 124                 | <256+ chars string>                    | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field team identifier is over 255 characters             |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | <256+ chars string>                    | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field sales person identifier is over 255 characters     |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | <256+ chars string> | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field booking reference is over 255 characters           |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | <256+ chars string> | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field operator booking identifier is over 255 characters |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | <256+ chars string> | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field operator leg identifier is over 255 characters     |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | <256+ chars string> | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field tail number is over 255 characters                 |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | <256+ chars string> | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field aircraft type code is over 255 characters          |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | <256+ chars string> | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field aircraft type display name is over 255 characters  |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | <256+ chars string> | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field aircraft type name is over 255 characters          |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | <5+ chars string> | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field departure iata code is over 4 characters           |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | <5+ chars string> | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field departure icao code is over 4 characters           |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | <5+ chars string> | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field arrival iata code is over 4 characters             |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | <5+ chars string> | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | this is a remark      | 0200            | The value in field arrival icao code is over 4 characters             |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | <64001+ chars string> | this is a remark      | 0200            | The value in field notes is over 64000 characters                     |
      | 123                 | 124                 | "cd1b3676-0fa4-4206-8d00-869bd66b9032" | "beca0b8b-b3e0-48bf-a3a7-04c958758592" | ABC                 | 2018-05-15T19:45:00Z | 123                 | 123                 | VCP               | SBCP              | VCR               | SBGR              | A320                | Airbus 320          | Airbus A320neo      | PR-AXT              | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z      | 2018-10-17T09:00:00Z          | 2018-10-17T09:00:00Z        | 2018-10-17T09:00:00Z       | 2018-10-17T09:00:00Z     | 2018-10-17T09:00:00Z         | 2018-10-17T09:00:00Z       | please leave a note   | <64001+ chars string> | 0200            | The value in field remarks is over 64000 characters                   |

Feature: Creating a Booking as a Broker/Operator - Multileg
  @Booking @CDTest @E2ETest

  Scenario Outline: Creating a Multileg flight

  #Step 1 - Acceptance Criteria Scenario 3: receives a _bookingStart_ and returns a _update_
    Given An *authenticated* Broker
    And Broker wants to request a MULTILEG booking
    And Payload is filled with correct data
    And One operator is running
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway returns an 'update' to the Broker with the 'transactionID'
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'
    And Gateway receives a success response from the Operator


  #Step 2 - Scenario: operator confimed the flight and gateway return to the broker
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator verifies the 'update'
    And Operator sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with the 'transactionID'
    And Gateway sends the 'update' to the *correct* Broker with the new  'transactionID'
    And Gateway receives a success response from the Broker

  #Step 3 - Test Scenario: broker receives the operator's payload back from the gateway. Then the broker needs to send it back to the gateway (as an "ack").
  #No changes were made, so consensus was reached (verified on Gateway),and it will not need to be forwarded to the operator
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker accepted the 'update'
    And Broker sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the response
    And Gateway verifies the payload
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with a new 'transactionID'
    And Gateway finishes transactions since consensus was reached

  #Step 4 - Scenario: Operator requests a multileg booking
    Given An authenticated Operator
    And Operator wants to request a MULTILEG booking
    And Payload is filled with correct data
    And Operator sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway returns an 'update' to the Operator with the 'transactionID'
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the correct Broker with the 'transactionID'
    And Gateway receives a success response from the Broker

  #Step 5 - Scenario: broker confimed the flight and gateway return to the operator
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker verifies the 'update'
    And Broker sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns a 'update' to the Broker with a new 'transactionID'
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'
    And Gateway receives a success response from the Operator

  #Step 6 - Test Scenario: operator receives the broker's payload back from the gateway. Then the operator needs to send it back
  #to the gateway (as an "ack"). No changes were made, so consensus was reached (verified on Gateway),and it will not need to be forwarded to the broker
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator accepted the 'update'
    And Operator sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the response
    And Gateway verifies the response
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with a new 'transactionID'
    And Gateway finishes transactions since consensus was reached

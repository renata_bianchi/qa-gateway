Feature: Updating a Booking Request as an Operator - Add more Legs

  Scenario Outline: Updating a booking as an Operator

  //Step 1 - Acceptance Criteria Scenario 1: Operator not authenticated
    Given An unauthenticated Operator
    And Operator *sent* an 'updateStart' (ADD)
    And Payload is filled with correct data
    When API Gateway receives the request
    And Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the Operator

  //Step 2 - Acceptance Scenario 2: wrong booking ID
    Given An authenticated Operator
    And Payload is filled with a wrong booking ID
    And Operator sent an 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Operator

  //Step 3 - Acceptance Scenario 3: wrong broker ID
    Given An authenticated Operator
    And Payload is filled with an Operator ID that doesn't belong to it
    And Operator sent the 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Operator

  //Step 4 - Acceptance Criteria Scenario 4: booking update received
    Given An authenticated Operator
    And Payload is filled with an UPDATE REQUEST to add a new leg
    And Operator sent the 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway accepts the request
    And Gateway returns an 'update' to the Operator with the 'transactionID'
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the Broker with the 'transactionID'
    And Gateway receives a success response from the Broker

  //Step 5 - Scenario: update received, verified and posetively responded by the broker
    Given An authenticated Broker
    And Broker received the 'update'
    And The 'update' was accepted by the Broker (ADDED)
    And Broker sent the 'update'
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with the new 'transactionID'
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'
    And Gateway receives a success response from the Operator

  //Step 6 - Scenario: updates accepted and verified by the Gateway. Consensus reached
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator accepted the 'update'
    And Operator sent the 'update'
    When Gateway receives the response
    And Gateway verifies the response
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with the new 'transactionID'
    And Gateway finishes transactions since consensus was reached

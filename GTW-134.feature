Feature: Creating a Booking as a Broker - Happy Flow

  Scenario Outline: Create a new booking as a broker

  //Step 1 - Acceptance Criteria Scenario 3: receives a bookingStart and returns an update
    Given An authenticated Broker
    And Payload is filled with correct data
    And One operator is running
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms user authentication
    And Gateway accepts the request
    And Gateway returns an 'update' to the Broker with the transactionID
    And Gateway srotes the request on the blockchain
    And Gateway sends the 'update' to the Operator with the transactionID
    And Gateway receives a success response from the Operator

  //Step 2 - Acceptance Criteria Scenario 4: two or more operators running on the same webhooks
    Given An authenticated Broker
    And Payload is filled with correct data
    And Two or more operators are running in the same webhooks
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway stores the request on the blockchain
    And Gateway returns an 'update' to the broker with the transactionID
    And Gateway sends the 'update' to the correct Operator in the correct webhook with the transactionID
    And Gateway receives a success response from the Operator

  //Step 3 - Acceptance Criteria Scenatio 5: two or more operators running on different webhooks
    Given An authenticated Broker
    And Payload is filled with correct data
    And Two or more Operatos are running in different webhooks
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway stores the request on the blockchain
    And Gateway returns an 'update' to the Broker with the transactionID
    And Gateway sends the 'update' to the correct Operator in the correct webhook with the transactionID
    And Gateway receives a success response from the Operator

  //Step 4 - Scenario: operator confirmed the flight and gateway returns it to the broker
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator verifies the 'update'
    And Operator sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns an update to the Operator with a new transactionID
    And Gateway sends the update to the correct Broker with the new transactionID
    And Gateway receives a success response from the Broker

  //Steps 5 - Test Scenario: broker receives the operator's payload back from the gateway. Then the broker needs to send it back
  //to the gateway (as an "ack"). No changes were made, so consensus was reached (verified on Gateway),and it will not need to be forwarded to the operator
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker accepted the 'update'
    And Broker sends the 'update' back to the Gateway with their own identifiers
    When Gateway receives the response
    And Gateway verifies the payload
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with a new transactionID
    And Gateway finishes transactions since consensus was reached

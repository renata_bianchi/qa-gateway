Feature: Updating a Booking Request as a Broker - Amend one or more Legs

  Scenario Outline: Amend one or more legs as a Broker

  //Step 1 - Acceptance Criteria Scenario 1: broker not authenticated
    Given An unauthenticated Broker
    And Payload is filled with correct data
    And Broker sent an 'updateStart' (UPDATE)
    When Gateway receives the request
    And API Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the Broker

  //Step 2 - Acceptance Criteria Scenario 2: wrong booking ID
    Given An authenticated Broker
    And Payload is filled with a *wrong booking ID (requesterBookingId)*
    And Broker sent an 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  //Step 3 - Acceptance Criteria Scenario 3: wrong broker ID
    Given An authenticated Broker
    And Payload is filled with a Broker ID that doesn't belong to him
    And Broker sent the 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  //Step 4 - Acceptance Criteria Scenario 4: booking update accepted
    Given An authenticated Broker
    And Payload is filled with an UPDATE REQUEST to amend two legs
    And Broker sends the 'updateStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway accepts the request
    And Gateway returns an 'update' to the Broker with the 'transactionID'
    And Gateway stores the request on the blockhain
    And Gateway sends the 'update' to the Operator with the 'transactionID'
    And Gateway receives a success response from the Operator

  //Step 5 - Scenario: update received, verified and responded positively by the operator
    Given An authenticated Operator
    And Operator received the 'update'
    And The 'update' is accepted by the Operator (UPDATED)
    And The 'update' confirmation was sent by the Operator
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with the new 'transactionID'
    And Gateway sends the 'update' to the correct Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker

  //Step 6 - Scenario: update received, verified and responded negatively by the operator
    Given An authenticated Operator
    And Operator received the 'update'
    And The 'update' was denied by the Operator (UPDATE_DENIED)
    And The update' confirmation was sent by the Operator
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Operator with the new 'transactionID'
    And Gateway sends the 'update' to the correct Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker //(update is *denied* and has to be started again)

  //Step 7 - Scenario: updates accepted and verified by the Gateway. Consensus reached
    Given An authenticated Broker
    And Broker received the 'update'
    And The 'update' was accepted by the Broker (UPDATED)
    And The 'update' confirmation was sent by the Broker
    When Gateway receives the response
    And Gateway verifies the response
    And The request and response are in consensus
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the Broker with the new 'transactionID'
    And Gateway finishes the transactions since consensus was reached
    And Legs were successfully amended
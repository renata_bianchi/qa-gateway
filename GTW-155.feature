Feature: Creating a Booking as an Operator - Alternative Flows

  Scenario Outline: Alternative flows for creating a booking as an Operator'

  //Step 1 - Acceptance Criteria Scenario 1: unauthenticated operator
    Given An unauthenticated Operator
    And Payload is filled with correct data
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the user

  //Step 2 - Scenario: broker is offline
    Given An authenticated Operator
    And Payload is filled with correct data
    And Broker is offline
    And Broker sent a 'bookingStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway confirms the user authentication
    And Gateway accepts the request
    And Gateway returns an 'update' to the Operator with the 'transactionID'_
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the Broker with the 'transactionID'
    And Gateway receives an error response from the Broker

  //Step 3 - Scenario: operator is offline
    Given An authenticated Broker
    And Broker received the 'update'
    And Broker verifies the 'update'
    And Broker sent the 'update' back to the Gateway with their own identifiers
    When Gateway receives the request
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with a new 'transactionID'
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'
    And Gateway receives a fail response from the Operator

  //Step 4 -  Test Scenario: operator receives the broker's payload back from the gateway. Then the operator needs to send it back to the gateway (as an "ack").
  //Changes were made, so no consensus was reached (verified on Gateway),and it will need to be forwarded to the broker and so on, until consensus is reached
    Given An authenticated Operator
    And Operator received the 'update'
    And Operator verifies the 'update'
    And Operator sent the 'update' back to the Gateway
    When Gateway receives the response
    And Gateway verifies the response from both Operator and Broker
    And The request and response are NOT in consensus
    Then Gateway notices the divergent responses
    And Gateway sends the 'update' to the correct Operator with the new 'transactionID'



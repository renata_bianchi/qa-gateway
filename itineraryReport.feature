Feature: Create a new itinerary as an Operator

  Scenario Outline: Creating a new itinerary as an Operator
    Given An authenticated Operator
    And Payload is filled with:
      | Broker Booking ID | Broker ID  | Operator Booking ID | Operator ID  | Itinerary Status  | Broker Leg ID | Departure IATA Code | Arrival IATA Code | Other Data  |
      | <brokerBookingID> | <brokerID> | <operatorBookingID> | <operatorID> | <itineraryStatus> | <brokerLegID> | <departureIataCode> | <arrivalIataCode> | <otherData> |
    And User sends an itineraryReport
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns code "<GatewayResponse>"

    Examples: Valid Data
      | brokerBookingID | brokerID | operatorBookingID | operatorID | itineraryStatus | brokerLegID | departureIataCode | arrivalIataCode | otherData   | GatewayResponse |
      | 123             | 1234     | 456               | 4567       | FINAL           | 123         | VCP               | VCR             | comment 123 | 0200            |

    Examples: Missing Data
      | brokerBookingID | brokerID | operatorBookingID | operatorID | itineraryStatus | brokerLegID | departureIataCode | arrivalIataCode | otherData   | GatewayResponse |
      | <NULL>          | 1234     | 456               | 4567       | FINAL           | 123         | VCP               | VCR             | comment 123 | 0500            |
      | 123             | <NULL>   | 456               | 4567       | FINAL           | 123         | VCP               | VCR             | comment 123 | 0500            |
      | 123             | 1234     | <NULL>            | 4567       | FINAL           | 123         | VCP               | VCR             | comment 123 | 0500            |
      | 123             | 1234     | 456               | <NULL>     | FINAL           | 123         | VCP               | VCR             | comment 123 | 0500            |
      | 123             | 1234     | 456               | 4567       | FINAL           | <NULL>      | VCP               | VCR             | comment 123 | 0500            |
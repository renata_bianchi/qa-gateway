Feature: Updating a Booking Request as a Broker - Add more Legs

  Scenario Outline: Updating a booking as a Broker

  //Step 1 - Acceptance Criteria Scenario 1: broker not authenticated
    Given An unauthenticated broker
    And Broker sent an 'updateStart (ADD)'
    And Payload is filled with correct data
    When Gateway receives the request
    And API Gateway verifies user's authentication
    Then Gateway notices the unauthenticated user
    And Gateway returns a fail response to the Broker

  //Step 2 - Acceptance Scenario 2: wrong booking ID
    Given An authenticated broker
    And Payload is filled with a wrong booking ID
    And Broker sent an 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  //Step 3 - Acceptance Scenario 3: wrong broker ID
    Given An authenticated broker
    And Payload is filled with a Broker ID that doesn't belong to it
    And Broker sent the 'updateStart'
    When Gateway receives the request
    And API Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway returns a fail response to the Broker

  //Step 4 - Acceptance Criteria Scenario 4: booking update received
    Given An authenticated broker
    And Payload is filled with an UPDATE REQUEST to add a new leg
    And Broker sent the 'updateStart'
    When Gateway receives the request
    And Gateway verifies user's authentication
    And Gateway verifies the payload
    Then Gateway accepts the request
    And Gateway returns an 'update' to the Broker with the 'transactionID'
    And Gateway stores the request on the blockchain
    And Gateway sends the 'update' to the Operator with the 'transactionID'
    And Gateway receives a success response from the Operator

  //Step 5 - Scenario: update received, verified and posetively responded by the operator
    Given An authenticated operator
    And Operator received the 'update'
    And The 'update' was accepted by the Operator (ADDED)
    And Operator sent the 'update'
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with the new 'transactionID'
    And Gateway sends the 'update' to the correct Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker

  //Step 6 - Scenario: update received, verified and negatively responded by the operator
    Given An authenticated operator
    And Operator received the 'update'
    And The 'update' was denied by the Operator (ADD_DENIED)
    And Operator sent the 'update'
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with the new 'transactionID'
    And Gateway sends the 'update' to the *correct* Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker (update is denied and has to be started again)

  //Step 7 - Scenario: update received, verified and negatively responded by the operator
    Given An authenticated Operator
    And Operator received the 'update'
    And The 'update' was denied by the Operator (ADD_DENIED)
    And Operator sent the 'update'
    When Gateway receives the response
    Then Gateway stores the response on the blockchain
    And Gateway returns an 'update' to the operator with the new 'transactionID'
    And Gateway sends the 'update' to the *correct* Broker with the new 'transactionID'
    And Gateway receives a success response from the Broker (update is *denied* and has to be started again)
